install:
	   pip install --upgrade pip &&\
		pip install -r code/requirements.txt

test:
	python -m pytest -vv code/test_app.py

testprint:
	python -m pytest -v -s -vv code/test_app.py

all: install test testprint

